--------------------------------------
-- Atmosphere: Core

-- License: MIT

-- Credits: xeranas
--------------------------------------

local core = {}

core.registered_atmospheres = {}
core.active_atmospheres = {}

-- Registers atmosphere
core.register_atmosphere = function(ao)
	if ao.code == nil then
		minetest.log("error", "Unable to register atmosphere without code")
		ao.status = "ERROR"
		return
	end
	ao.status = "REGISTERED"
	table.insert(core.registered_atmospheres, ao)
end

-- Deregisters atmosphere
core.deregister_atmosphere = function(ao)
	ao.status = "DEREGISTERED"
	-- Remove atmosphere from registered atmosphere list
	if #core.registered_atmospheres == 0 then
		return
	end
	for key, ao in ipairs(core.registered_atmospheres) do
		if ao.code == code then
			table.remove(core.registered_atmospheres, key)
			return
		end
	end

	-- Remove atmosphere from active atmosphere list (in case it was activated)
	if #core.active_atmospheres == 0 then
		return
	end
	for key, ao in ipairs(core.active_atmospheres) do
		if ao.code == code and ao.pos == pos then
			table.remove(core.active_atmospheres, key)
			return
		end
	end
end

-- Resolves active atmosphere object by code
core.resolve_active_ao = function(code)
	if #core.active_atmospheres == 0 then
		return nil
	end

	for key, ao in ipairs(core.active_atmospheres) do
		if ao.code == code then
			return ao
		end
	end	
end

-- Resolves registered atmosphere object by code
core.resolve_registered_ao = function(code)
	if #core.registered_atmospheres == 0 then
		return nil
	end

	for key, ao in ipairs(core.registered_atmospheres) do
		if ao.code == code then
			return ao
		end
	end	
end

-- Checks if atmosphere active.
-- If given position not nil also checks if position effected by atmosphere
core.is_atmosphere_active = function(code, pos)
	local ao = core.resolve_active_ao(code)
	if ao == nil then
		return false
	end

	if pos ~= nil then
		return ao.is_area_effected(pos)
	end
	return true
end

-- Removes atmosphere from active atmosphere list
core.remove_active_ao = function(code, pos)
	if #core.active_atmospheres == 0 then
		return
	end

	for key, ao in ipairs(core.active_atmospheres) do
		if ao.code == code and ao.pos == pos then
			table.remove(core.active_atmospheres, key)
			return
		end
	end
end

-- Activate atmosphere on demand
core.activate_atmosphere = function(code, pos)
	if core.resolve_active_ao(code) ~= nil then
		-- already activated	
		return
	end
	local ao = core.resolve_registered_ao(code)
	core.activate(0, ao, pos)
end

-- Activates atmosphere
core.activate = function(dtime, ao, pos)
	ao.status = "ACTIVE"
	ao.pos = pos
	table.insert(core.active_atmospheres, ao)

	if ao.on_start == nil then
		return
	end
	ao.on_start(dtime, pos)
end

-- Deactivates atmosphere
core.deactivate = function(dtime, ao, pos)
	ao.status = "INACTIVE"
	if #core.active_atmospheres == 0 then
		return
	end

	core.remove_active_ao(ao.code, ao.pos)
	if ao.on_end == nil then
		return
	end
	ao.on_end(dtime)
end

-- AO execute nil-safe method wrapper
core.execute = function(dtime, ao, player)
	if ao.execute == nil then
		return
	end
	if ao.is_area_effected ~= nil and ao.is_area_effected(player:getpos()) == false then
		return
	end
	ao.execute(dtime, player)
end

-- Triggers atmosphere start based on start_check callback result
core.start_check = function(dtime, ao)
	if ao.start_check == nil then
		return
	end
	if ao.start_check(dtime) == true then
		core.activate(dtime, ao, nil)
	end
end

-- Triggers atmosphere end based on end_check callback result
core.end_check = function(dtime, ao)
	if ao.end_check == nil then
		return
	end
	if ao.end_check(dtime) == true then
		core.deactivate(dtime, ao, nil)
	end
end

core.on_step_callback = function(dtime, ao)
	if ao.on_step_callback == nil then
		return
	end
	ao.on_step_callback(dtime)
end

--------------------------
-- Global step function --
--------------------------
local delaytimer = 0
local total_dtime = 0
local interval = 1
minetest.register_globalstep(function(dtime)
	delaytimer = delaytimer + dtime
	if delaytimer < interval then
		total_dtime = total_dtime + dtime
		return
	end
	total_dtime = delaytimer
	delaytimer = 0

	if #minetest.get_connected_players() == 0 then
		-- no actual players, do nothing.
		return
	end

	if #core.registered_atmospheres == 0 then
		-- no registered atmospheres, do nothing.
		return
	end

	-- Check for if active atmosphere is about to start
	for i, ao in ipairs(core.registered_atmospheres) do
		core.on_step_callback(total_dtime, ao)
		core.start_check(total_dtime, ao)
	end

	if #core.active_atmospheres == 0 then
		-- no active atmospheres, do nothing.
		return
	end

	-- Loop through players and execute active atmosphere for them
	for i, player in ipairs(minetest.get_connected_players()) do
		for j, ao in ipairs(core.active_atmospheres) do
			core.execute(total_dtime, ao, player)
		end
	end

	-- Check for if active atmosphere is about to end
	for i, ao in ipairs(core.active_atmospheres) do
		core.end_check(total_dtime, ao)
	end
end)

return core
